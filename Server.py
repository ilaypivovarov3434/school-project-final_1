# импортируем библиотеки

import socket
import time
import random
from _thread import *
import threading

win_data = None
prev_win_data = None
count_wins = 0
# определения и переменные
print_lock = threading.Lock()
send_data = 'Hello'
win_number = random.randint(0, 100)

print(win_number)

def threaded(c):
    global win_number, count_wins

        # данные, полученные от клиента

    data = c.recv(1024)

    # использование данных в игре

    data = int(data)

    # проверить и отправить обратно результат игроку

    if data < win_number:
        send_data = "Заданное число больше!"
        c.sendall((str(send_data)).encode("utf-8"))
    elif data > win_number:
        send_data = "Заданное число меньше!"
        c.sendall((str(send_data)).encode("utf-8"))
    elif data == win_number:
        send_data = "Вы угадали!"
        c.sendall((str(send_data)).encode("utf-8"))
        global win_data, prev_win_data

        # сравнение с предыдушим результатом

        win_data = c.recv(1024)
        if prev_win_data == None:
            prev_win_data = win_data
            win_number = random.randint(0, 100)
            print(win_number)
            count_wins = count_wins + 1
        else:
            if win_data < prev_win_data:
                c.sendall((str("Вы побили рекорд!")).encode("utf-8"))
                prev_win_data = win_data
            if win_data > prev_win_data:
                c.sendall((str("Ваш результат хуже предыдушего!")).encode("utf-8"))
            if win_data == prev_win_data:
                c.sendall((str("Ваш результат сопадает с рекордом!")).encode("utf-8"))
            count_wins = count_wins + 1
            if count_wins % 10 == 0:
                prev_win_data = None
            win_number = random.randint(0, 100)
            print(win_number)
    c.close()

# основная функция для соединения
def Main():
    host = ""

    port = 2323

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    s.bind((host, port))

    print("socket binded to port", port)

    # перевести сокет в режим прослушивания (кол - во игроков на сервере)

    s.listen(10)

    print("socket is listening")

    # вечный цикл
    while True:
        c, addr = s.accept()

        print('Connected to :', addr[0], ':', addr[1])
        # Запустить новый поток и вернуть его идентификатор
        start_new_thread(threaded, (c,))

    c.close()

# запуск основной функции
if __name__ == '__main__':
    Main()