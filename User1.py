# импортируем библиотеки
import socket
import time

sum = 0

# основная функция
def Main():

    # local host IP '127.0.0.1'

     while True:
        host = '127.0.0.1'
        global sum
    # Определяем порт, к которому подключиться

        port = 2323

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # подключиться к серверу

        # задержка по времени (на всякий случай)

        time.sleep(1)
        print("Введите число:")

        # сообшение отправленное на сервер


        message = str(input())

        # обработка исключений

        while True:
            try:
                int(message)
                break

            except:
                print("Введено не число! Повторите попытку:")
                message = str(input())
        # присоединяемся к серверу


        s.connect((host, port))

        # сообшение отправленно на сервер

        s.send(message.encode('utf-8'))

        # сообщение получено с сервера

        data = s.recv(1024)

        sum = sum + 1

        # проверка на победу

        if str(data.decode('utf-8')) != 'Вы угадали!':
            print(str(data.decode('utf-8')))
        else:
            print("Спасибо за игру!!")
            print("Затраченно:",sum)
            s.send(str(sum).encode("utf-8"))
            data = s.recv(1024)
            sum = 0
            print(str(data.decode("utf-8")))
            print("                      ")



            print("Хотите продолжить? Для продолжения введите:'да', для окончания: лубой другой символ.")
            cou_answer = str(input())
            if cou_answer == "да":
                continue
            else:
                print("Bye!")
                break
            s.close()


# запуск игры

print("Игра: 'Тест на удачу'. Угадайте число от 0 до 100!")
if __name__ == '__main__':
    Main()
